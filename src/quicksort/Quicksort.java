/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package quicksort;

import java.util.Scanner;

/**
 *
 * @author Daniel
 */
public class Quicksort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Scanner sc = new  Scanner(System.in);
        //String listaStr = sc.nextLine();
        int list[] = {1,2,3,4,5,6,7,8,9,10};
        quicksort(list);
        for(int i=0; i<list.length; i++){
            System.out.print(list[i]+" ");
        }
        System.out.println();
        
        int list2[] = {1000, 10, 7, 8, 9, 30, 900, 1, 5, 6, 20};
        quicksort(list2);
        for(int i=0; i<list2.length; i++){
            System.out.print(list2[i]+" ");
        }
        System.out.println();
        
        int list3[] = {10,9,8,7,6,5,4,3,2,1};
        quicksort(list3);
        for(int i=0; i<list3.length; i++){
            System.out.print(list3[i]+" ");
        }
        System.out.println();
    }
    
    public static void quicksort(int a[]) {
        quicksort(a, 0, a.length - 1);
    }
    
    private static void quicksort(int a[], int left, int right){
        int i, j, mid, pivote;
        mid = (left+right)/2;
        pivote = a[mid];
        i = left; 
        j = right;
        
        do{
            while(a[i] < pivote) i++;
            while(a[j] > pivote) j--;
            if(i<=j){
                int aux = a[i];
                a[i] = a[j];
                a[j] = aux;
                i++; 
                j--;
            }
        }while(i<=j);
        
        if(left < j) 
            quicksort(a,left, j);
        if(right > i) 
            quicksort(a,i, right);
    }
    
}
